# kudoapp
Give Kudos to your teammate! 

### 1. Clone the project on your system
```https://gitlab.com/ae16b112/kudoapp.git```

### 2. Go inside in the project directory.
```cd kudoapp```

### 3. Create virtual environment
```virtualenv kudoenv```

### 4. Activate environment
```source kudoenv/bin/activate```

### 5. Configure the database based on your local database credentials in settings.py

### 6. Run server
``` python manage.py runserver```

### Features
1. Only logged in users can give kudos.
2. Logged in user can only give kudo to people within their organization.
3. An individual can give upto total 3 kudos in a week.
4. Dashboard - Dashboard to see who has given, how many kudos to the logged in user.
5. Kudos do not accumulate: if a user doesn't give out their kudos, they do not get 6 to give out next week, only 3..
6. register, login and logout pages


### Endpoints
1. Home - http://127.0.0.1:8000/kudos/
2. Dashboard - http://127.0.0.1:8000/kudos/dashboard
3. givekudo - http://127.0.0.1:8000/kudos/givekudo
4. login - http://127.0.0.1:8000/users/login
5. register - http://127.0.0.1:8000/users/register
6. logout - http://127.0.0.1:8000/users/logout