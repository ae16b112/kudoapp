from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    organization_name=models.CharField(max_length=100)

    class Meta:
        db_table = "users"
        verbose_name = "App User"
        verbose_name_plural = "App Users"
        
    def __str__(self):
        return "{username} - {organization_name}".format(
            username=self.username,
            organization_name=self.organization_name
        )
