from django.contrib.auth import authenticate,login, logout
from django.shortcuts import render, redirect
from users.models import User
from .forms import RegisterUserForm
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages


@csrf_exempt
def register(request):
    if request.method == "POST":
        form = RegisterUserForm(request.POST)

        if form.is_valid():
            user = form.save()
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            messages.success(request, 'Account created for username %s' % user.username)
            return redirect('home')

    else:
        form = RegisterUserForm()

    context = {'form': form}
    return render(request, 'users/register.html', context)


@csrf_exempt
def user_login(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = User.objects.get(username=form.cleaned_data.get("username"))
            login(request, user)
            messages.success(request, "Hello {}, you are logged in now!!".format(user.username))
            return redirect('home')
        else:
            messages.error(request, 'Invalid username or password')
            return redirect('login')
    else:
        form = AuthenticationForm()

    context = {'form': form}
    return render(request, 'users/login.html', context)


def user_logout(request):
    logout(request)
    messages.success(request, "You have been logged out")
    return redirect('home')