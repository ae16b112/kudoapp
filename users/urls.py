from django.urls import path
from rest_framework import routers

from users import views

router = routers.DefaultRouter()

urlpatterns = [
    path('register', views.register, name='register'),
    path('login', views.user_login, name='login'),
    path('logout', views.user_logout, name='logout'),
]

urlpatterns += router.urls