from django.contrib import admin

from kudos.models import Kudo

# Register your models here.
admin.site.register(Kudo)
