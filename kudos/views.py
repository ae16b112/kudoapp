from django.shortcuts import render
from django.http import HttpResponse
from .forms import KudoForm
from users.models import User
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from datetime import datetime, timedelta
from .models import Kudo

# Create your views here.

def home(request):
    return render(request, 'kudos/home.html')

@csrf_exempt
def givekudo(request):
    if request.user.is_authenticated:
        form = KudoForm(request)
        from_user=User.objects.get(pk=request.user.id)
        if request.method == "POST":
            form = KudoForm(request, data=request.POST)
            if form.is_valid():
                today = datetime.now().date()
                start = today - timedelta(days=today.weekday())
                end = start + timedelta(days=6)
                to_user=User.objects.get(pk=form.data.get('collegue_name'))
                kudos_already_given=Kudo.objects.filter(from_user=from_user).exclude(kudo_date__lt=start).filter(kudo_date__lte=end)
                count_kudos_already_given=sum([kudo.kudo_count for kudo in kudos_already_given])
                kudos_to_be_given=form.data.get('kudo_count')
                total_kudos=count_kudos_already_given + int(kudos_to_be_given)
                if (total_kudos) > 3:
                    messages.info(request, 'You can only give 3 Kudos per week')
                else:
                    kudo_details=Kudo.objects.create(from_user=from_user, to_user=to_user, content=form.data.get("message"), kudo_count=form.data.get("kudo_count"))
                    kudo_details.save()
                    messages.success(request, "You have successfully given {kudo_count} kudos to - {name}".format(
                        kudo_count=form.data.get("kudo_count"),
                        name=to_user.username,
                    ))
        context = {'form': form}
        return render(request, 'kudos/kudo.html', context)

    return render(request, 'kudos/home.html')


def dashboard(request):
    if request.user.is_authenticated:
        to_user=User.objects.get(pk=request.user.id)
        today=datetime.now().date()
        start=today - timedelta(days=today.weekday())
        end=start + timedelta(days=6)
        kudo_data=Kudo.objects.filter(to_user=to_user).exclude(kudo_date__lt=start).filter(kudo_date__lte=end)
        dashboard_data=[{'from_user':kudo.from_user.username, 
                         'kudo_count': kudo.kudo_count, 
                         'message':kudo.message,
                         'date_posted': str(kudo.kudo_date)} for kudo in kudo_data]
        context = {'dashboard': dashboard_data}
        return render(request, 'kudos/dashboard.html', context)
    return render(request, 'kudos/home.html')