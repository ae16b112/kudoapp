from django import forms
from users.models import User

class KudoForm(forms.Form):

    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(KudoForm, self).__init__(*args, **kwargs)

        user_details = User.objects.get(pk=self.request.user.id)
        org_members = User.objects.exclude(pk=self.request.user.id).filter(
            organization_name=user_details.organization_name)
        self.fields["collegue_name"] = forms.TypedChoiceField(choices=[(member.pk,
                                                                        User.objects.get(id=member.pk).get_username())
                                                                       for member in org_members],
                                                              coerce=int)

    collegue_name = forms.TypedChoiceField(
        choices=(), help_text="Select colleague to whom you want to give kudo.")
    kudo_count = forms.ChoiceField(choices=[(x, x) for x in range(1, 4)])
    message = forms.CharField(
        max_length=100, help_text="Write a message for your team member.")